### JAVASCRIPT 

What will the code below output to the console and why?

```javascript
var myObject = {
    foo : "bar",
    func:function() {
        var self=this;
        console.log("outer func: this.foo = "+this.foo);
        console.log("outer func: self.foo = "+self.foo);
        (function(){
            console.log("inner func: this.foo = " +this.foo);
            console.log("inner func: self.foo = " +self.foo);   
        }());
    }
};

myObject.func();
```
Console:  
  outer func: this.foo = bar  
  outer func: self.foo = bar  
  inner func: this.foo = undefined  
  inner func: self.foo = bar  

  En los 2 primeros casos tanto el this como self pertenecen al mismo scope por lo tanto devuelven lo mismo(bar).
  En las funciones internas se crea un contexto interno donde se ejecuta por lo tanto el this es diferente, pertenece a este nuevo contexto donde no esta definido foo. En cambio la variable self no existe en el scope interno por eso lo busca en un scope superior donde encuentra el self que en su contexto tiene definido foo.


a. What is a Promise?
  A promise is an object that may produce a future value.  
  A promise has three possibles states: fulfilled, rejected or pending.  
  That function takes two parameters, resolve and reject.  
    resolve: return a value to the callback .then()  
    reject: return a Error object to the calllback .catch()  

b. What is ECMAScript?
  ECMAScript is a standard script language.  
  ES6 or ES2015 is the last standard javascript  

c. What is NaN? What is its type? How can you reliably test if a value is equal to NaN ?
  NaN literally stand for "not a number". NaN is "invalid number," "failed number," or even "bad number".  
  The type of NaN (not a number) is number.  
  We use the global utility named isNaN() and it tell us if value is NaN or not.  

d. What will be the output when the following code is executed? Explain.

```javascript 
console.log(false=='0')
console.log(false==='0')
```
  Console:  
    true  
    false  
  ```javascript
  console.log(false=='0') //implicit coercion
  //the data type is converted for the comparison. '0' is equal to false

  console.log(false==='0')
  //value and data type are compared
  ```
### Node

Explain what does "event-driven, non-blocking I/O" means in Javascript.

En node se facilita mucho el manejo de peticiones asíncronas para peticiones y eventos de entrada/salida. Tanto con callbacks como promesas o async/await que nativamente ya soporta javascript.
No se produce bloqueos porque no se queda esperando la finalizacion de la peticion, sino que al terminarse se ejecuta un callback enviado o devuelve el resultado de una promesa para seguir con ese hilo de la ejecución.
De esta manera se pueden hacer muchas mas peticiones concurrentemente
