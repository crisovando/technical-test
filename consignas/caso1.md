1)  
   f.bar: is not a function  
   f.baz: 7  
   f.biz: a is not defined  

2)  
```javascript
  var Foo = function( a ) {
      this.bar = function() {
        return a;
      };
      this.baz = function() {
        return a;
      };
    };
```
3)
```javascript
  var Foo = function( a ) {
      this.a = a;

      this.bar = function() {
        return a;
      };
      this.baz = function() {
        return a;
      };
    };

  Foo.prototype = {
        biz: function() {
          return this.a;
        }
      };
```