var endorsements = [
  { skill: 'css', user: 'Bill' },
  { skill: 'javascript', user: 'Chad' },
  { skill: 'javascript', user: 'Bill' },
  { skill: 'css', user: 'Sue' },
  { skill: 'javascript', user: 'Sue' },
  { skill: 'html', user: 'Sue' }
];


var groupBySkill = function(collection) {
  return collection.reduce(function(rv, x) {
    let item = rv.filter(item => item.skill == x.skill)[0];
    if (item) {
      item.user += ',' + x.user;
      item.count++;
    }
    else {
      x.count = 1;
      rv.push(x);
    }
    return rv;
  }, []);
};

console.log(groupBySkill(endorsements));