Tengo las siguientes funciones:

```javascript
function buscarEnFacebook(texto, callback) {
  /* Hace algunas cosas y las guarda en "result" */
  if (result.error) {
    callback(error, result.error)
  } else {
    callback(null, result.data);
  }
}
function buscarEnGithub(texto, callback) {
  /* Hace algunas cosas y las guarda en "result" */
  if (result.error) {
    callback(error, result.error)
  } else {
    callback(null, result.data);
  }
}
```

  - ¿Qué debería hacer para usar la funcionalidad con promesas y no callbacks?

```javascript
function buscarEnFacebook(texto) {
  return new Promise((resolve, reject) => {
    /* Hace algunas cosas y las guarda en "result" */
    if (result.error) reject(result.error);

    resolve(result.data);
  })
}
function buscarEnGithub(texto) {
  return new Promise((resolve, reject) => {
    /* Hace algunas cosas y las guarda en "result" */
    if (result.error) reject(result.error);

    resolve(result.data);
  })
}
```

  - ¿Podés replicar la siguiente API?

```javascript
buscador('hola')
.facebook()
.github()
.then((data) => {
  // data[0] = data de Facebook
  // data[1] = data de GitHub
})
.catch(error => {
  // error[0] = error de Facebook
  // error[1] de GitHub
})
```
Respuesta:  
```javascript

function Busqueda(text) {
  let self = this;
  this.text = text;
  this.arrPromises = [];
  this.errors = [];
  this.values = [];
  
  this.then = function(callback) {
    return new Promise(async(resolve, reject) => {
      for (let promise of this.arrPromises) {
        await promise.then(async resolvedData => await this.values.push(resolvedData), (err) => this.errors.push(err))
        
        if (this.values.length + this.errors.length === this.arrPromises.length) {
          if (this.values.length > 0 ) callback(this.values)
        }
      }
      if (this.errors.length > 0) {
        reject(this.errors);
      } 
    })
  };
}
Busqueda.prototype.facebook = function() {
    this.arrPromises.push(new Promise((resolve, reject) => {
        reject(`facebook ${this.text} error`);
        resolve('facebook '+this.text);
    }))
    return this;
}

Busqueda.prototype.gitHub = function() {
    this.arrPromises.push(new Promise((resolve, reject) => {
        reject(`gitHub ${this.text} error`);
        resolve('gitHub '+this.text);
    }))
    return this;
}

const buscador = (text) => {
    return new Busqueda(text)
}
```

  - y a la solución anterior
    - ¿Cómo podrías agregarle otra búsqueda? <br/>
```javascript
Busqueda.prototype.google = function() {
		this.arrPromises.push(new Promise((resolve, reject) => {
  			reject(`google ${this.cadena} error`);
  			resolve('google '+this.cadena);
		}))
		return this;
}
```
    - ¿Cómo solucionas el problema de si una API entrega un error, mientras las otras devuelven data? <br/>
      Diseñe una clase Busqueda que implementa su propia version de then para ejecutar. En caso de las que se ejecutaron correctamente se devolverán los valores en el callback, las que dieron error seran enviadas al catch para ser manipulado.